INSTRUCTIONS:
Write a SQL query to insert a new employee with the first name 'Jane', last name 'Doe', middle name 'Marie', birthday '1990-01-01', and address '456 Elm Street'.

Write a SQL query to retrieve the first name, last name, and birthday of all employees in the table.

Write a SQL query to retrieve the number of employees whose last name starts with the letter '
D'.

Write a SQL query to retrieve the first name, last name, and address of the employee with the highest ID number.

Write a SQL query to update the address of the employee with the first name 'John' to '123 Main Street'.

Write a SQL query to delete all employees whose last name starts with the letter 'D'.
